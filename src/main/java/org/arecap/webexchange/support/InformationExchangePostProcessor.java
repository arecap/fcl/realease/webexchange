/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.webexchange.support;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.arecap.webexchange.InformationExchange;
import org.arecap.webexchange.InformationExchangeContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public class InformationExchangePostProcessor implements BeanPostProcessor {

    /** Logger available to subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    private InformationExchangeJwtProperties jwtProperties;

    private ApplicationContext applicationContext;

    private ObjectMapper objectMapper = new ObjectMapper();

    public InformationExchangePostProcessor(InformationExchangeJwtProperties jwtProperties, ApplicationContext applicationContext) {
        this.jwtProperties = jwtProperties;
        this.applicationContext = applicationContext;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if(bean != null) {
            InformationExchange informationExchangeInfo = AnnotationUtils.findAnnotation(bean.getClass(), InformationExchange.class);
            if(informationExchangeInfo != null) {
                ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                if(servletRequestAttributes == null) {
                    logger.warn("Early initialisation of InformationExchange bean type [" + bean.getClass() + "]. RequestContext not initialized!");
                } else {
                    HttpServletResponse response = servletRequestAttributes.getResponse();
                    if(InformationExchangeContext.class.isAssignableFrom(response.getClass())) {
                        InformationExchangeContext informationExchangeContext = (InformationExchangeContext) response;
                        informationExchangeContext.refresh();
                        String beanJti = informationExchangeContext.getApplicationJti(beanName);
                        if(beanJti != null && beanJti.trim().length() > 0) {
                            Claims beanClaims = parseInformationExchangeClaims(informationExchangeInfo, beanJti);
                            return objectMapper.convertValue(beanClaims, bean.getClass());
                        }
                        logger.debug("Not jti store in InformationExchangeContext for beanName ["+beanName+"]");
                    } else {
                        logger.debug("ServletRequestAttributes response not wrapped!");
                    }
                }

            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    protected Claims parseInformationExchangeClaims(InformationExchange info, String jti) {
        Environment env = applicationContext.getEnvironment();
        JwtParser jwtParser = Jwts.parser();
        return info.algorithm().length() > 0 ?
                jwtParser.setSigningKey(env.resolvePlaceholders(info.key()).getBytes()).parseClaimsJws(jti).getBody() :
                jwtParser.parseClaimsJwt(jti).getBody();
    }

}
