/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.webexchange.support;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 * Support bean for read jwt application.properties configurations
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public class InformationExchangeJwtProperties {

    @Value("${information.exchange.signature.key}")
    private String signatureKey;

    @Value("${information.exchange.signature.algorithm}")
    private String sa;

    @Value("${information.exchange.issuer}")
    private String issuer;

    @Value("${spring.application.name}")
    private String applicationName;

    public String getSignatureKey() {
        return signatureKey;
    }

    public SignatureAlgorithm getSa() {
        return SignatureAlgorithm.forName(sa);
    }

    public String getIssuer() {
        return issuer == null ? applicationName : issuer;
    }

    public boolean isSigned() {
        try {
            getSa();
        } catch (SignatureException se) {
            return false;
        }
        return true;
    }

}
