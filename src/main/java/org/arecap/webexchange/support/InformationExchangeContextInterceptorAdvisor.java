/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.webexchange.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.impl.JwtMap;
import org.aopalliance.intercept.MethodInvocation;
import org.arecap.webexchange.InformationExchange;
import org.arecap.webexchange.InformationExchangeContext;
import org.springframework.aop.support.DelegatingIntroductionInterceptor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 *
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public class InformationExchangeContextInterceptorAdvisor extends DelegatingIntroductionInterceptor implements InformationExchangeContext {

    private static final long serialVersionUID = 0l;

    final static String ACCESS_RESPONSE_DATA_METHOD_NAME = "getOutputStream";

    private final InformationExchangeJwtProperties jwtProperties;

    private final ApplicationContext applicationContext;

    private final ObjectMapper objectMapper;

    private transient boolean needRefresh = true;

    private Claims informationExchange;

    public InformationExchangeContextInterceptorAdvisor(InformationExchangeJwtProperties jwtProperties, ApplicationContext applicationContext, ObjectMapper objectMapper) {
        this.informationExchange = new DefaultClaims();
        this.needRefresh = true;
        this.jwtProperties = jwtProperties;
        this.applicationContext = applicationContext;
        this.objectMapper = objectMapper;
    }

    public InformationExchangeContextInterceptorAdvisor(InformationExchangeJwtProperties jwtProperties, ApplicationContext applicationContext) {
        this(jwtProperties, applicationContext, new ObjectMapper());
    }

    @Override
    protected Object doProceed(MethodInvocation mi) throws Throwable {
        if(mi.getMethod().getName().equalsIgnoreCase(ACCESS_RESPONSE_DATA_METHOD_NAME)) {
            commit();
        }
        return super.doProceed(mi);
    }

    @Override
    public void refresh() {
        if(!needRefresh) {
            return;
        }
        needRefresh = false;
        clear();
        JwtParser parser = getJwtParser();
        String ieJwt = getIeJwt();
        if(ieJwt != null && ieJwt.trim().length() > 0) {
            putAll(parser.isSigned(ieJwt) ?
                    parser.parseClaimsJws(ieJwt).getBody() : parser.parseClaimsJwt(ieJwt).getBody());
        }
    }

    protected String getIeJwt() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String ieJwt = request.getHeader(INFORMATION_EXCHANGE);
        if(ieJwt == null || ieJwt.trim().length() == 0) {
            Cookie ieJwtCookie = WebUtils.getCookie(request, INFORMATION_EXCHANGE);
            if(ieJwtCookie != null) {
                ieJwt = ieJwtCookie.getValue();
            }
        }
        return ieJwt;
    }

    protected JwtParser getJwtParser() {
        JwtParser jwtParser = Jwts.parser();
        if(jwtProperties.isSigned()) {
            jwtParser.setSigningKey(jwtProperties.getSignatureKey().getBytes());
        }
        return jwtParser;
    }

    @Override
    public void commit() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        setApplicationInformationExchange();
        setSubject(jwtProperties.getIssuer());
        setAudience(request.getRequestURL().toString());
        setIssuedAt(new Date());
        JwtBuilder builder = jwtProperties.isSigned() ? getJwsBuilder(jwtProperties.getSa(), jwtProperties.getSignatureKey()) : Jwts.builder();
        setId(builder.setClaims(this.informationExchange).compact());
        writeHeader(request, ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse());

    }

    protected void writeHeader(HttpServletRequest request,  HttpServletResponse response) {
        response.setHeader(INFORMATION_EXCHANGE, getId());
        WebExchangeUtil.setCookieValue(request, response, INFORMATION_EXCHANGE, getId());
    }

    protected void setApplicationInformationExchange() {
        try {
            Map<String, String> applicationIeJwtMap = getApplicationIeJwtMap();
            for (String informationExchangeSubject : applicationIeJwtMap.keySet()) {
                put(informationExchangeSubject, applicationIeJwtMap.get(informationExchangeSubject));
            }
        }catch (Exception e) {

        }
    }

    protected JwtBuilder getJwsBuilder(String algorithm, String signature) {
        return getJwsBuilder(SignatureAlgorithm.forName(algorithm), signature);
    }

    protected JwtBuilder getJwsBuilder(SignatureAlgorithm sa, String signature) {
        return Jwts.builder().signWith(sa, signature.getBytes());
    }

    protected Claims convertToClaims(String subject, JwtMap informationExchange, InformationExchange info) {
        Environment env = applicationContext.getEnvironment();
        return convertToClaims(subject, informationExchange, info.algorithm().length() > 0 ?
                getJwsBuilder(env.resolvePlaceholders(info.algorithm()), env.resolvePlaceholders(info.key())) : Jwts.builder());
    }

    protected Claims convertToClaims(String subject, JwtMap informationExchange, JwtBuilder builder) {
        Claims claims = new DefaultClaims(informationExchange);
        claims.setSubject(subject);
        claims.setIssuer(jwtProperties.getIssuer());
        claims.setAudience(jwtProperties.getIssuer());
        claims.setIssuedAt(new Date());
        claims.setId(builder.setClaims(claims).compact());
        return claims;
    }

    protected JwtMap getJwtMap(Object claim) {
        return objectMapper.convertValue(claim, JwtMap.class);
    }

    protected Map<String, String> getApplicationIeJwtMap() {
        Map<String, Object> ieBeans = applicationContext.getBeansWithAnnotation(InformationExchange.class);
        Map<String,  String> applicationIeJwtMap = new HashMap<String, String>();
        for(String ieBeanName: ieBeans.keySet()) {
            InformationExchange informationExchangeInfo = AnnotationUtils.
                    findAnnotation(ieBeans.get(ieBeanName).getClass(), InformationExchange.class);
            applicationIeJwtMap
                    .put(ieBeanName, convertToClaims(ieBeanName, getJwtMap(ieBeans.get(ieBeanName)),
                            informationExchangeInfo).getId());
        }
        return applicationIeJwtMap;
    }

    @Override
    public String getApplicationJti(String subject) {
        return get(subject, String.class);
    }

    @Override
    public String getUrl() {
        return getAudience();
    }

    @Override
    public String getIssuer() {
        return this.informationExchange.getIssuer();
    }

    @Override
    public Claims setIssuer(String iss) {
        return this.informationExchange.setIssuer(iss);
    }

    @Override
    public String getSubject() {
        return this.informationExchange.getSubject();
    }

    @Override
    public Claims setSubject(String sub) {
        return this.informationExchange.setSubject(sub);
    }

    @Override
    public String getAudience() {
        return this.informationExchange.getAudience();
    }

    @Override
    public Claims setAudience(String aud) {
        return this.informationExchange.setAudience(aud);
    }

    @Override
    public Date getExpiration() {
        return this.informationExchange.getExpiration();
    }

    @Override
    public Claims setExpiration(Date exp) {
        return this.informationExchange.setExpiration(exp);
    }

    @Override
    public Date getNotBefore() {
        return this.informationExchange.getNotBefore();
    }

    @Override
    public Claims setNotBefore(Date nbf) {
        return this.informationExchange.setNotBefore(nbf);
    }

    @Override
    public Date getIssuedAt() {
        return this.informationExchange.getIssuedAt();
    }

    @Override
    public Claims setIssuedAt(Date iat) {
        return this.informationExchange.setIssuedAt(iat);
    }

    @Override
    public String getId() {
        return this.informationExchange.getId();
    }

    @Override
    public Claims setId(String jti) {
        return this.informationExchange.setId(jti);
    }

    @Override
    public <T> T get(String claimName, Class<T> requiredType) {
        return this.informationExchange.get(claimName, requiredType);
    }

    @Override
    public int size() {
        return this.informationExchange.size();
    }

    @Override
    public boolean isEmpty() {
        return this.informationExchange.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return this.informationExchange.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return this.informationExchange.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return this.informationExchange.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        return this.informationExchange.put(key, value);
    }

    @Override
    public Object remove(Object key) {
        return this.informationExchange.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ?> m) {
        this.informationExchange.putAll(m);
    }

    @Override
    public void clear() {
        this.informationExchange.clear();
    }

    @Override
    public Set<String> keySet() {
        return this.informationExchange.keySet();
    }

    @Override
    public Collection<Object> values() {
        return this.informationExchange.values();
    }

    @Override
    public Set<Entry<String, Object>> entrySet() {
        return this.informationExchange.entrySet();
    }

}
