/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.webexchange.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultIntroductionAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.ClassUtils;
import org.springframework.web.filter.RequestContextFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
public class InformationExchangeFilter extends RequestContextFilter {

    /** Logger available to subclasses */
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private InformationExchangeJwtProperties jwtProperties;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        InformationExchangeContextInterceptorAdvisor delegatingServletInterceptor =
                new InformationExchangeContextInterceptorAdvisor(jwtProperties, applicationContext, objectMapper);
        HttpServletResponse wrapResponse = wrapHttpResponse(response, delegatingServletInterceptor);
        super.doFilterInternal(request, wrapResponse, filterChain);
    }

    protected HttpServletResponse wrapHttpResponse(HttpServletResponse response, InformationExchangeContextInterceptorAdvisor delegatingServletInterceptorAdvisor) {
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTargetClass(response.getClass());
        proxyFactory.setTarget(response);
        proxyFactory
                .setInterfaces(ClassUtils
                        .getAllInterfacesForClass(
                                response.getClass(),
                                applicationContext.getClassLoader()));
        proxyFactory.addAdvisor(new DefaultIntroductionAdvisor(delegatingServletInterceptorAdvisor));
        proxyFactory.setOptimize(true);
        return (HttpServletResponse) proxyFactory.getProxy();
    }

}
