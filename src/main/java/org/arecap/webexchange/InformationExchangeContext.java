/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.arecap.webexchange;

import io.jsonwebtoken.Claims;

/**
 *
 * The {@link org.arecap.webexchange.InformationExchange} components
 * JWT Claims serializable informational exchange context
 *
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
public interface InformationExchangeContext extends Claims {

    /*
     *
     * The HTTP cookie header or url encoding parameter default name
     *
     */
    String INFORMATION_EXCHANGE = "Information-Exchange";

    /*
     *
     * Refresh the Information Exchange Context Claims
     * This should be use at request handler before Spring Context
     *  Request Scope beans PostProcessor
     *
     */
    void refresh();

    /*
     *
     * Writes information in HttpRequest cookie header or url
     *
     */
    void commit();

    String getApplicationJti(String subject);

    String getUrl();

}
