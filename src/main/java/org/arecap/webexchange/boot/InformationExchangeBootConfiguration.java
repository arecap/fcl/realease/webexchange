/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.webexchange.boot;

import org.arecap.webexchange.support.InformationExchangeFilter;
import org.arecap.webexchange.support.InformationExchangeJwtProperties;
import org.arecap.webexchange.support.InformationExchangePostProcessor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.*;

import static org.springframework.beans.factory.config.BeanDefinition.ROLE_SUPPORT;

/**
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 *
 */
@Configuration
@ComponentScan("org.springframework.web.servlet.exchange.")
@PropertySource("classpath:informationexchange.properties")
public class InformationExchangeBootConfiguration implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Bean
    @Role(ROLE_SUPPORT)
    public InformationExchangeJwtProperties getInformationExchangeJwtProperties() {
        return new InformationExchangeJwtProperties();
    }

    @Bean
    @Role(ROLE_SUPPORT)
    public InformationExchangeFilter getInformationExchangeFilter() {
        return new InformationExchangeFilter();
    }

    @Bean
    @Role(ROLE_SUPPORT)
    public InformationExchangePostProcessor getInformationExchangePostProcessor() {
        return new InformationExchangePostProcessor(getInformationExchangeJwtProperties(), applicationContext);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
